
log_level = "DEBUG"

data_dir = "/tmp/client"

name = "client"

client {
    enabled = true
    network_interface = "enp0s8"
    servers = ["172.16.10.11:4647"]
}

# Modify our port to avoid a collision with server1
ports {
    http = 5656
}

advertise {
  http = "{{ GetInterfaceIP `enp0s8` }}"
  rpc  = "{{ GetInterfaceIP `enp0s8` }}"
  serf = "{{ GetInterfaceIP `enp0s8` }}"
}
          # GetInterfaceIP \`eth1\` MAYBE DRUGOI SHTRIH

# Disable the dangling container cleanup to avoid interaction with other clients
plugin "docker" {
  config {
    gc {
      dangling_containers {
        enabled = false
      }
    }
  }
}

