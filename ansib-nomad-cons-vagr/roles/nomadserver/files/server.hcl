
log_level = "DEBUG"

data_dir = "/tmp/server1"

server {
    enabled = true
    bootstrap_expect = 1
}

client {
  enabled           = true
  network_interface = "enp0s8"
  servers           = ["172.16.10.11"]
  #servers           = ["172.16.10.21", "172.16.10.22"]
}

advertise {
  http = "{{ GetInterfaceIP `enp0s8` }}"
  rpc  = "{{ GetInterfaceIP `enp0s8` }}"
  serf = "{{ GetInterfaceIP `enp0s8` }}"
}
            #GetInterfaceIP \`eth1\`

plugin "raw_exec" {
  config {
    enabled = true
  }
}

